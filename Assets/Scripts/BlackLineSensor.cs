﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackLineSensor : MonoBehaviour
{
    public bool state;

    public LayerMask blackLine;
    private RaycastHit hit;

    private void Update()
    {
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, blackLine))
        {
            state = true;
        }
        else
            state = false;
    }

}
