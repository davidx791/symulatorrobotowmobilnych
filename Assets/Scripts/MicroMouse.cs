﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct MapCell
{                    //dla orientacji polnocnej
    public bool left;//czy ma lewa sciane
    public bool right;//czy ma prawa sciane
    public bool top;//czy ma sciane z przodu
    public bool bottom;//czy ma sciane z tylu

    public bool visited;//czy dana komorka byla juz odwiedzona

    public int weight;//waga dla algorytmu floodfill
}


public class MicroMouse : MonoBehaviour
{
    enum Position : int
    {
        North,
        East,
        South,
        West,
    };

    private MapCell[,] mazeMap;//tablica przechowujaca informacje o labiryncie

    private int x;//pozycja x robota w labiryncie
    private int y;//pozycja y robota w labiryncie
    private int currentWeight;//waga aktualnej komorki 
    private Position currentPosition;//aktualna pozycja
    private Vector2[] possibleMoves;//kolejno wspolrzedne ruchu do przodu, w prawo i w lewo

    private int nrOfTurns;//ilosc zakretow podczas tej symulacji
    private int nrOfSteps;//ilosc krokow
    private int maxWeight = 99999;//zmienna pomocnicza do algorytmu FloodFillExtended

    private DistanceSensor LeftSensor;//sensory robota
    private DistanceSensor FrontLeftSensor;
    private DistanceSensor FrontMiddleLeftSensor;
    private DistanceSensor FrontMiddleRightSensor;
    private DistanceSensor FrontRightSensor;
    private DistanceSensor RightSensor;

    private string algorithmName;//nazwa aktualnie uzywanego algorytmu

    private GameObject StartSolvingPoint;//punkt startowy danego labiryntu

    private void Start()//inicjalizacja czujnikow oraz wybranego algorytmu poruszania sie
    {

        StartSolvingPoint = GameObject.Find("StartSolvingPoint");

        x = 0;
        y = 1;
        currentWeight = 1;
        currentPosition = Position.North;
        possibleMoves = new Vector2[3];

        nrOfTurns = 0;
        nrOfSteps = 0;
        algorithmName = "ExploreMap";//RightHand; FloodFill
        //algorithmName = "RightHand";

        LeftSensor = transform.GetChild(0).GetComponent<DistanceSensor>();//przypisanie czyjnikow do zmiennych
        FrontLeftSensor = transform.GetChild(1).GetComponent<DistanceSensor>();
        FrontMiddleLeftSensor = transform.GetChild(2).GetComponent<DistanceSensor>();
        FrontMiddleRightSensor = transform.GetChild(3).GetComponent<DistanceSensor>();
        FrontRightSensor = transform.GetChild(4).GetComponent<DistanceSensor>();
        RightSensor = transform.GetChild(5).GetComponent<DistanceSensor>();

        MapInit();//inicjalizacja tablicy mapy
        InvokeRepeating(algorithmName, 0.5f, 0.1f);//rozpoczecie exploracji 
    }

    //**********************************************//
    //Sekcja poruszania sie

    private void TurnRight()//skret w prawo
    {
        currentPosition++;
        if ((int)currentPosition > 3)
        {
            int tmp = (int)currentPosition % 4;
            currentPosition = (Position)tmp;//wyliczenia kolejnej pozycji przy zmianie rotacji
        }
        nrOfTurns++;//dodanie kolejnego zakretu
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90, transform.rotation.eulerAngles.z);
    }

    private void TurnAround()//obrot o 180 stopnii, zawrocenie
    {
        currentPosition += 2;
        if ((int)currentPosition > 3)
        {
            int tmp = (int)currentPosition % 4;
            currentPosition = (Position)tmp;
        }
        nrOfTurns++;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 180, transform.rotation.eulerAngles.z);
    }

    private void TurnLeft()//skret w lewo
    {
        currentPosition += 3;
        if ((int)currentPosition > 3)
        {
            int tmp = (int)currentPosition % 4;
            currentPosition = (Position)tmp;
        }
        nrOfTurns++;
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90, transform.rotation.eulerAngles.z);
    }

    private void MoveForward()//przemieszczenie robota do przodu
    {
        transform.position += transform.forward * SimulationControl.segmentWidth;
        nrOfSteps++;
    }

    //**********************************************//
    //Sekcja algorytmow
    //**********************************************//

    private void RightHand()
    {
        if (!RightSensor.state)//kiedy nie ma sciany po lewej
            TurnRight();
        else if (!FrontMiddleRightSensor.state) { }//kiedy nie ma sciany po srodku
        else if (!LeftSensor.state)//kiedy nie ma sciany po lewej
            TurnLeft();
        else
            TurnAround();

        MoveForward();

    }

    //**********************************************//
    //

    private void ExploreMap()//eksploracja labiryntu w celu zapamietania informacji
    {

        currentWeight = mazeMap[x, y].weight;//waga aktaulnej komorki
        mazeMap[x, y].visited = true;

        CheckCellState();//pobranie informacji o przeszkodach, polozeniu i mozliwych ruchach z tej komorki

        //wybranie kierunku na podstawie danych z sensora oraz zapamietanych informacji
        if (!FrontMiddleRightSensor.state && !mazeMap[(int)possibleMoves[0].x, (int)possibleMoves[0].y].visited) { }
        else if (!RightSensor.state && !mazeMap[(int)possibleMoves[1].x, (int)possibleMoves[1].y].visited)
            TurnRight();
        else if (!LeftSensor.state && !mazeMap[(int)possibleMoves[2].x, (int)possibleMoves[2].y].visited)
            TurnLeft();
        else if (FrontRightSensor.state && RightSensor.state && LeftSensor.state)
            TurnAround();
        else//losowy wybor kierunku
        {
            int random = (int)Random.Range(0, 1.99f);
            if (random == 0)
            {
                if (!RightSensor.state)
                    TurnRight();
                else if (!FrontRightSensor.state) { }
                else if (!LeftSensor.state)
                    TurnLeft();
            }
            else
            {
                if (!LeftSensor.state)
                    TurnLeft();
                else if (!FrontRightSensor.state) { }
                else if (!RightSensor.state)
                    TurnRight();
            }

        }

        MoveForward();//przemieszczenie robota

        //zmiania orientacji na mapie
        if (currentPosition == Position.North)
            y++;
        else if (currentPosition == Position.East)
            x++;
        else if (currentPosition == Position.South)
            y--;
        else if (currentPosition == Position.West)
            x--;

    }

    //**********************************************//
    //

    private void FillMap(int x, int y, int weight)//weplnianie mapy wagami po eksploracji
    {

        if (x < 0 || y < 0 || x > 15 || y > 15)
            return;
        if (mazeMap[x, y].weight == -1)
            mazeMap[x, y].weight = weight;
        else
            return;
        //Debug.Log("" + x + " " + y + ":" + mazeMap[x, y].weight);
        if (!mazeMap[x, y].top)
            FillMap(x, y + 1, mazeMap[x, y].weight + 1);
        if (!mazeMap[x, y].right)
            FillMap(x + 1, y, mazeMap[x, y].weight + 1);
        if (!mazeMap[x, y].left)
            FillMap(x - 1, y, mazeMap[x, y].weight + 1);
        if (!mazeMap[x, y].bottom)
            FillMap(x, y - 1, mazeMap[x, y].weight + 1);

    }

    //**********************************************//
    //

    private void FillMapExtended(int x, int y, int weight, Position PreviousPosition)//weplnianie mapy wagami po eksploracji
    {
        int top = 3;
        int right = 3;
        int left = 3;
        int bottom = 3;

        if (PreviousPosition == Position.North)
            top = 1;
        else if (PreviousPosition == Position.East)
            right = 1;
        else if (PreviousPosition == Position.West)
            left = 1;
        else
            bottom = 1;

        if (x < 0 || y < 0 || x > 15 || y > 15)
            return;
        if (mazeMap[x, y].weight == -1)
            mazeMap[x, y].weight = weight;
        else
            return;
        //Debug.Log("" + x + " " + y + ":" + mazeMap[x, y].weight);
        if (!mazeMap[x, y].top)
            FillMapExtended(x, y + 1, mazeMap[x, y].weight + top, Position.North);
        if (!mazeMap[x, y].right)
            FillMapExtended(x + 1, y, mazeMap[x, y].weight + right, Position.East);
        if (!mazeMap[x, y].left)
            FillMapExtended(x - 1, y, mazeMap[x, y].weight + left, Position.West);
        if (!mazeMap[x, y].bottom)
            FillMapExtended(x, y - 1, mazeMap[x, y].weight + bottom, Position.South);

    }

    //**********************************************//
    //

    private void FloodFill()
    {
        currentWeight = mazeMap[x, y].weight;
        if (currentWeight == 1)
        {
            Debug.Log("Number of truns:" + nrOfTurns);
            Debug.Log("Number of steps:" + nrOfSteps);
            return;
        }
        CheckCellState();

        int min = Mathf.Min(mazeMap[(int)possibleMoves[0].x, (int)possibleMoves[0].y].weight, mazeMap[(int)possibleMoves[1].x, (int)possibleMoves[1].y].weight, mazeMap[(int)possibleMoves[2].x, (int)possibleMoves[2].y].weight);
        if (!FrontMiddleRightSensor.state && mazeMap[(int)possibleMoves[0].x, (int)possibleMoves[0].y].weight == min) { }
        else if (!RightSensor.state && mazeMap[(int)possibleMoves[1].x, (int)possibleMoves[1].y].weight == min)
            TurnRight();
        else if (!LeftSensor.state && mazeMap[(int)possibleMoves[2].x, (int)possibleMoves[2].y].weight == min)
            TurnLeft();
        else
        {
            if (!FrontRightSensor.state)
            { }
            else if (!RightSensor.state)
                TurnRight();
            else if (!LeftSensor.state)
                TurnLeft();
            else
                TurnAround();
        }
        MoveForward();

        if (currentPosition == Position.North)
            y++;
        else if (currentPosition == Position.East)
            x++;
        else if (currentPosition == Position.South)
            y--;
        else if (currentPosition == Position.West)
            x--;

    }

    //**********************************************//
    //

    private void FloodFillExtended()
    {

        currentWeight = mazeMap[x, y].weight;
        if (currentWeight == 1)
        {
            Time.timeScale = 0;
            Debug.Log("Number of truns:" + nrOfTurns);
            return;
        }
        CheckCellState();

        int front = maxWeight;
        int right = maxWeight;
        int left = maxWeight;

        if (!FrontRightSensor.state && mazeMap[(int)possibleMoves[0].x, (int)possibleMoves[0].y].weight > -1)
            front = mazeMap[(int)possibleMoves[0].x, (int)possibleMoves[0].y].weight;
        if (!RightSensor.state && mazeMap[(int)possibleMoves[1].x, (int)possibleMoves[1].y].weight > -1)
            right = mazeMap[(int)possibleMoves[1].x, (int)possibleMoves[1].y].weight;
        if (!LeftSensor.state && mazeMap[(int)possibleMoves[2].x, (int)possibleMoves[2].y].weight > -1)
            left = mazeMap[(int)possibleMoves[2].x, (int)possibleMoves[2].y].weight;

        int min = Mathf.Min(front - 2, right, left);

        if (!FrontRightSensor.state && front - 2 == min) { }
        else if (!RightSensor.state && right == min)
            TurnRight();
        else if (!LeftSensor.state && left == min)
            TurnLeft();
        else
        {
            if (!FrontRightSensor.state)
            { }
            else if (!RightSensor.state)
                TurnRight();
            else if (!LeftSensor.state)
                TurnLeft();
            else
                TurnAround();
        }
        MoveForward();

        if (currentPosition == Position.North)
            y++;
        else if (currentPosition == Position.East)
            x++;
        else if (currentPosition == Position.South)
            y--;
        else if (currentPosition == Position.West)
            x--;

    }

    //**********************************************//
    //

    private void CheckCellState()
    {

        if (currentPosition == Position.North)
        {
            mazeMap[x, y].top = FrontRightSensor.state;
            mazeMap[x, y].right = RightSensor.state;
            mazeMap[x, y].left = LeftSensor.state;
            mazeMap[x, y].bottom = false;

            if (!mazeMap[x, y].top)
            {
                possibleMoves[0].x = x;
                possibleMoves[0].y = y + 1;
            }
            if (!mazeMap[x, y].right)
            {
                possibleMoves[1].x = x + 1;
                possibleMoves[1].y = y;
            }
            if (!mazeMap[x, y].left)
            {
                possibleMoves[2].x = x - 1;
                possibleMoves[2].y = y;
            }

        }
        else if (currentPosition == Position.East)
        {
            mazeMap[x, y].top = LeftSensor.state;
            mazeMap[x, y].right = FrontRightSensor.state;
            mazeMap[x, y].left = false;
            mazeMap[x, y].bottom = RightSensor.state;

            if (!mazeMap[x, y].right)
            {
                possibleMoves[0].x = x + 1;
                possibleMoves[0].y = y;
            }
            if (!mazeMap[x, y].bottom)
            {
                possibleMoves[1].x = x;
                possibleMoves[1].y = y - 1;
            }
            if (!mazeMap[x, y].top)
            {
                possibleMoves[2].x = x;
                possibleMoves[2].y = y + 1;
            }

        }
        else if (currentPosition == Position.South)
        {
            mazeMap[x, y].top = false;
            mazeMap[x, y].right = LeftSensor.state;
            mazeMap[x, y].left = RightSensor.state;
            mazeMap[x, y].bottom = FrontRightSensor.state;

            if (!mazeMap[x, y].bottom)
            {
                possibleMoves[0].x = x;
                possibleMoves[0].y = y - 1;
            }
            if (!mazeMap[x, y].left)
            {
                possibleMoves[1].x = x - 1;
                possibleMoves[1].y = y;
            }
            if (!mazeMap[x, y].right)
            {
                possibleMoves[2].x = x + 1;
                possibleMoves[2].y = y;
            }

        }
        else if (currentPosition == Position.West)
        {
            mazeMap[x, y].top = RightSensor.state;
            mazeMap[x, y].right = false;
            mazeMap[x, y].left = FrontRightSensor.state;
            mazeMap[x, y].bottom = LeftSensor.state;

            if (!mazeMap[x, y].left)
            {
                possibleMoves[0].x = x - 1;
                possibleMoves[0].y = y;
            }
            if (!mazeMap[x, y].top)
            {
                possibleMoves[1].x = x;
                possibleMoves[1].y = y + 1;
            }
            if (!mazeMap[x, y].bottom)
            {
                possibleMoves[2].x = x;
                possibleMoves[2].y = y - 1;
            }

        }

    }

    //**********************************************//
    //

    private void MapInit()//inicjalizacja labiryntu i wag kolejnych komorek
    {

        mazeMap = new MapCell[16, 16];
        for (int i = 0; i < mazeMap.GetLength(0); i++)
        {

            for (int j = 0; j < mazeMap.GetLength(1); j++)
            {
                mazeMap[i, j].weight = -1;
                mazeMap[i, j].top = false;
                mazeMap[i, j].right = false;
                mazeMap[i, j].bottom = false;
                mazeMap[i, j].left = false;
                mazeMap[i, j].visited = false;
            }

        }
        /*
        mazeMap[0, 0].top = false;
        mazeMap[0, 0].right = true;
        mazeMap[0, 0].bottom = true;
        mazeMap[0, 0].left = true;
        mazeMap[0, 0].visited = true;
        mazeMap[0, 0].weight = 0;

        mazeMap[0, 1].weight = 1;
        */
    }

    //**********************************************//
    //

    public void StartFloodFill()//funkcja do obslugi startu algorytmu FloodFill
    {
        FillMap(8, 8, 0);

        x = 0;
        y = 1;
        currentPosition = Position.North;

        nrOfTurns = 0;
        nrOfSteps = 0;

        transform.rotation = Quaternion.Euler(0, 0, 0);
        transform.position = StartSolvingPoint.transform.position;

        CancelInvoke(algorithmName);
        algorithmName = "FloodFill";
        Time.timeScale = 1;
        SimulationControl.simulationTime = 0f;
        InvokeRepeating(algorithmName, 0.5f, 0.1f);
    }

    //**********************************************//
    //

    public void StartFloodFillExtended()//funkcja do obslugi startu algorytmu FloodFillExtended
    {

        FillMapExtended(8, 8, 0, Position.North);

        x = 0;
        y = 1;
        currentPosition = Position.North;

        nrOfTurns = 0;
        nrOfSteps = 0;

        transform.rotation = Quaternion.Euler(0, 0, 0);
        transform.position = StartSolvingPoint.transform.position;

        CancelInvoke(algorithmName);
        algorithmName = "FloodFill";
        Time.timeScale = 1;
        SimulationControl.simulationTime = 0f;
        InvokeRepeating(algorithmName, 0.5f, 0.1f);
    }

}
