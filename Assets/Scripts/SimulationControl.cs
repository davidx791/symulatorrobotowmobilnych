﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationControl : MonoBehaviour
{

    public static float simulationTime;
    public static float segmentWidth;

    private void Start()
    {
        simulationTime = 0f;
        segmentWidth = 13.1f;
    }

    private void Update()
    {
        simulationTime += Time.deltaTime;
    }

}
