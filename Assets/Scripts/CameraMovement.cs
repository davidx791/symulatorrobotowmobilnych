﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    public GameObject robot;

    private Vector3 offset;      

    private void Start()
    {    
        offset = transform.position - robot.transform.position;
    }

    private void LateUpdate()
    {
        transform.position = robot.transform.position + offset;
        //transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, robot.transform.rotation.eulerAngles.y, robot.transform.rotation.eulerAngles.z);
    }

}
