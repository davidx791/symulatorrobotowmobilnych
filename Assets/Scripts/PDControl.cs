﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PDControl : MonoBehaviour
{
    //private int error;//aktualny blad
    private float prevError;//poprzedni blad
    public float kP;//wzmocnienie P
    public float kD;//wzmocnienie D

    private void Start()
    {
        if(kP == 0)
            kP = 10f;
        if(kD == 0)
            kD = 200f;
    }

    public float PD(float error)//wyliczanie regulacji 
    {
        float derivative = error - prevError;
        prevError = error;

        return kP * error + kD * derivative;
    }
}
