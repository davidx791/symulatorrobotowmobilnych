﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//**************************************//
//Skrypt odpowiedzialny za poruszanie   //
//sie kamera.                           //
//**************************************//

public class MoveCamera : MonoBehaviour
{

    private bool isScrollingUp;//czy skrcollujemy

    public float speed = 30f;//predkosc ruchu
    public float minY = 7f;//minimalne zblizenie
    public float maxY = 50f;//maksymalne oddalenie
    public float scrollSpeed = 20f;
    public float boundary = 0;//granica po ktorej juz lapie przesuwanie

    //private float offset = 10f;//odleglosc camery od obiektu widzianego na scenie
    private float scroll;//wartosc scrolla
    private float targetPositionY;//aktualna pozycja po y


    private Vector2 positionLimit;
    private Vector3 pos;//akutalna pozycja kamery

    //private Camera Minimap;

    private void Start()
    {

        transform.position = new Vector3(transform.position.x, maxY, transform.position.z);
        //Minimap = GameObject.Find("MinimapCamera").GetComponent<Camera>();
        positionLimit = new Vector2(500f, 500f);
        targetPositionY = transform.position.y;

    }

    private void Update()
    {

        pos = transform.position;
        //Vector3 minimapPos = Minimap.transform.position;

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey("d") || Input.mousePosition.x >= Screen.width - boundary)
        {
            pos.x += Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey("a") || Input.mousePosition.x <= 0 + boundary)
        {
            pos.x -= Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey("w") || Input.mousePosition.y >= Screen.height - boundary)
        {
            pos.z += Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey("s") || Input.mousePosition.y <= 0 + boundary)
        {
            pos.z -= Time.deltaTime * speed;
        }


        scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll > 0 && isScrollingUp)
        {
            isScrollingUp = false;
            targetPositionY = pos.y;
        }
        else if (scroll < 0 && !isScrollingUp)
        {
            isScrollingUp = true;
            targetPositionY = pos.y;
        }

        targetPositionY -= scroll * scrollSpeed * 75f * Time.deltaTime;

        pos.x = Mathf.Clamp(pos.x, -positionLimit.x, positionLimit.x);
        targetPositionY = Mathf.Clamp(targetPositionY, minY, maxY);
        pos.z = Mathf.Clamp(pos.z, -positionLimit.y, positionLimit.y);

        if (pos.y - targetPositionY > 1f)
            pos.y -= scrollSpeed * Time.deltaTime;
        else if (pos.y - targetPositionY < -1f)
            pos.y += scrollSpeed * Time.deltaTime;

        //minimapPos.y = pos.y;
        //Minimap.transform.position = minimapPos;
        transform.position = pos;

    }

}
