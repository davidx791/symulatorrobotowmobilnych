﻿using UnityEngine;
using System.Collections;

public class RearWheelDrive : MonoBehaviour
{

    public WheelCollider LWheel;
    public WheelCollider RWheel;

    //public float maxTorque = 300;//maksymalna predkosc obrotowa kol
    public float LTorque;//predkosc obrotwa lewego kola
    public float RTorque;//predkosc obrotowa prawego kola
    public float speedRatio;
    public GameObject wheelShape;

    private void Start()
    {
        GameObject ws = GameObject.Instantiate(wheelShape);
        ws.transform.parent = LWheel.transform;
        ws = GameObject.Instantiate(wheelShape);
        ws.transform.parent = RWheel.transform;

        LWheel.ConfigureVehicleSubsteps(1, 12, 15);
        RWheel.ConfigureVehicleSubsteps(1, 12, 15);
    }
    /*
    private void Update()
    {

        LWheel.motorTorque = LTorque;
        RWheel.motorTorque = RTorque;

        WheelShapeControl(LWheel);
        WheelShapeControl(RWheel);

    }
    */
    private void WheelShapeControl(WheelCollider Wheel)
    {
        Quaternion q;
        Vector3 p;
        Wheel.GetWorldPose(out p, out q);

        Transform shapeTransform = Wheel.transform.GetChild(0);
        shapeTransform.position = p;
        shapeTransform.rotation = q;
    }

    public void PWM(float rV, float lV)
    {
        LTorque = lV*speedRatio;
        RTorque = rV*speedRatio;

        LWheel.motorTorque = LTorque;
        RWheel.motorTorque = RTorque;

        WheelShapeControl(LWheel);
        WheelShapeControl(RWheel);
    }

}
