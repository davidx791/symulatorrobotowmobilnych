﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIControl : MonoBehaviour
{
    private GameObject LineFollower;

    private GameObject SpawnPoint1;
    private GameObject SpawnPoint2;
    private GameObject SpawnPoint3;

    private Text TimeText;

    private Scene Scene;

    private void Start()
    {

        Scene = SceneManager.GetActiveScene();

        if (Scene.name == "TEST")
            return;

        LineFollower = GameObject.Find("LineFollower");

        SpawnPoint1 = GameObject.Find("SpawnPoint1");
        SpawnPoint2 = GameObject.Find("SpawnPoint2");
        SpawnPoint3 = GameObject.Find("SpawnPoint3");

        TimeText = GameObject.Find("Time").GetComponent<Text>();

    }

    private void Update()
    {
        if(TimeText != null)
            TimeText.text = SimulationControl.simulationTime + "";

        if(Input.GetKeyDown(KeyCode.Escape) && Scene.name != "TEST")
            SceneManager.LoadScene("TEST");
    }

    public void Reset()
    {
        SceneManager.LoadScene("TEST");
    }

    public void Stop()
    {
        Time.timeScale = 0;
    }

    public void Run()
    {
        Time.timeScale = 1;
    }

    public void ChangeToSpawn1()
    {
        SimulationControl.simulationTime = 0;
        LineFollower.transform.position = SpawnPoint1.transform.position;
        LineFollower.transform.rotation = SpawnPoint1.transform.rotation;
    }

    public void ChangeToSpawn2()
    {
        SimulationControl.simulationTime = 0;
        LineFollower.transform.position = SpawnPoint2.transform.position;
        LineFollower.transform.rotation = SpawnPoint2.transform.rotation;
    }

    public void ChangeToSpawn3()
    {
        SimulationControl.simulationTime = 0;
        LineFollower.transform.position = SpawnPoint3.transform.position;
        LineFollower.transform.rotation = SpawnPoint3.transform.rotation;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

}
