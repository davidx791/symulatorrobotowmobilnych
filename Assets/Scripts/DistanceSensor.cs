﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceSensor : MonoBehaviour
{

    public bool state;
    private float distance;

    public LayerMask maze;
    private RaycastHit hit;

    private void Start()
    {
      
        distance = 8f;
    }
    
    private void Update()
    {
        
        if (Physics.Raycast(transform.position, transform.up, out hit, distance, maze))
        {
            Debug.DrawLine(transform.position, transform.position + (transform.up*distance), Color.red);
            state = true;
        }
        else
            state = false;
    }
    /*
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Maze")
            state = true;
        else
            state = false;
    }
    */
}
