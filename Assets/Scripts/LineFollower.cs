﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineFollower : MonoBehaviour
{

    private int speed;//predkosc zmiany polozenia

    [SerializeField]
    private int nrOfSensors;//liczba dostepnych sensorow
    private int weightsOffset;

    private float error;//do obliczenia wag
    private float prevError;//poprzedni blad w razie wypadniecia z trasy lub kata prostego
    private float control;//obliczona regulacja
    private float procSpeed;
    //private bool readingADC;//czy czujniki sa czytane

    private List<GameObject> Sensors;//lista sensorow robota
    private bool[] stateOfSensors;//stany robotow

    private void Start()
    {
        speed = 100;
        procSpeed = 0.01f;

        if(nrOfSensors == 0)
            nrOfSensors = 7;
        //readingADC = false;

        weightsOffset = (int)(nrOfSensors / 2);

        Sensors = new List<GameObject>();//inicjalizacja listy sensorow
        stateOfSensors = new bool[nrOfSensors];

        for (int i = 0; i < nrOfSensors; i++)
        {
            Sensors.Add(transform.GetChild(i).gameObject);
        }

        InvokeRepeating("ReadADC", procSpeed, procSpeed);
        InvokeRepeating("Move", procSpeed, procSpeed);
    }

    private void ReadADC()//funkcja zczytujaca dane z czujnika oraz obliczajaca odchylenie
    {

        int state;//stan aktualnie badanego czujnika
        float currentError = 0;
        float balance = 0;//suma stanow

        for (int i = 0; i < nrOfSensors; i++)//odczyt każdego czujnika w pętli
        {
 
            if (Sensors[i].GetComponent<BlackLineSensor>().state)
            {
                stateOfSensors[i] = true;
                state = 1;
            }
            else
            {
                stateOfSensors[i] = false;
                state = 0;
            }
            
            currentError += (state * (i - weightsOffset) * 10);//obliczanie bledu zgodnie z wagami np. -30 -20 -10 0 10 20 30
            balance += state;//dodanie balansu

        }

        if (balance != 0)//wyliczanie aktualnego błędu
        {
            error = currentError / balance;
            prevError = error;
        }
        else
        {
            if (prevError < weightsOffset * -10 + 10)    //linia ostatanio widziana po lewej stronie - ustalamy ujemny błąd większy od błędu skrajnego lewego czujnika 
                error = (weightsOffset + 1) * -10;
            else if (prevError > weightsOffset * 10 - 10)//linia ostatanio widziana po prawej stronie - analogicznie do powyższego 
                error = (weightsOffset + 1) * 10;
            else                    //przerwa w linii - trzeba jechać prosto 
                error = 0;
        }

    }

    private void Move()//funkcja odpowiedzialna za poruszanie sie
    {
        control = transform.GetComponent<PDControl>().PD(error);//wyliczanie regulacji z PD
        transform.GetComponent<RearWheelDrive>().PWM(speed + control, speed - control);//PWM silnika
    }

}
